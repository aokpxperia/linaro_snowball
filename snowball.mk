# The snowball product that is specialized for snowball.
$(call inherit-product, device/linaro/common/common.mk)
$(call inherit-product, device/linaro/snowball/device.mk)

PRODUCT_BRAND := snowball
PRODUCT_DEVICE := snowball
PRODUCT_NAME := snowball
